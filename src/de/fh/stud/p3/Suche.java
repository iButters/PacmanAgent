package de.fh.stud.p3;

import de.fh.pacman.PacmanPercept;
import de.fh.pacman.enums.PacmanAction;
import de.fh.pacman.enums.PacmanTileType;
import de.fh.stud.p1.Knoten;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Suche {
	/*
	 * TODO Praktikum 3 [1]: Erweitern Sie diese Klasse um die notwendigen
	 * Attribute und Methoden um eine Tiefensuche durchführen zu können.
	 * Die Erweiterung um weitere Suchstrategien folgt in Praktikum 4.
	 */
	private Knoten loesungsKnoten;

	private PacmanPercept pacmanPercept;

	private List<Knoten> openList = new ArrayList<Knoten>();

	private List<Knoten> closedList = new ArrayList<Knoten>();

	private Stack<PacmanAction> pacmanActionList = new Stack<PacmanAction>();
	public PacmanAction getNextAction(){
		if(this.pacmanActionList.size() != 0){
			return this.pacmanActionList.pop();
		}
		else {
			return null;
		}
	}

	public Suche(Knoten loesungsKnoten, PacmanPercept pacmanPercept){
		this.loesungsKnoten = loesungsKnoten;
		this.pacmanPercept = pacmanPercept;
	}
	/*
	 * TODO Praktikum 4 [1]: Erweitern Sie diese Klasse um weitere Suchstrategien (siehe Aufgabenblatt)
	 * zu unterstützen.
	 */
	
	public Knoten start() {
		/*
		 * TODO Praktikum 3 [2]: Implementieren Sie hier den Algorithmus einer Tiefensuche.
		 */
		Knoten startKnoten = new Knoten(pacmanPercept.getView(), pacmanPercept.getPosition().getX(), pacmanPercept.getPosition().getY());
		knotenEinfuegen(startKnoten);

		while (!openList.isEmpty()) {
			Knoten expansionsKandidat = this.popOpenlist();
			this.pushCloselist(expansionsKandidat);

			if (expansionsKandidat.isKnotenZiel(this.loesungsKnoten)) {
				int sizeResult = loesungsweg(expansionsKandidat);

				System.out.println("Länge des Weges: " + sizeResult);
				System.out.println("OpenList: " + this.openList.size() + " - ClosedList: " + this.closedList.size());
				return expansionsKandidat;
			}
			else {
				expandiereKnoten(expansionsKandidat);
			}
		}

		return null;
	}

	private void expandiereKnoten(Knoten vorgaenger) {
		List<Knoten> nachfolger = vorgaenger.expand();

		for(Knoten nachfolgerKnoten : nachfolger){
			boolean isInClosedList = false;

			for (Knoten closedListKnoten : closedList) {
				if (nachfolgerKnoten.isKnotenGleich(closedListKnoten)) {
					isInClosedList = true;
				}
			}

			if (!isInClosedList) {
				knotenEinfuegen(nachfolgerKnoten);
			}
		}
	}


	private void knotenEinfuegen(Knoten expansionsKandidat) {
		this.openList.add(0, expansionsKandidat);
	}

	private int loesungsweg(Knoten loesungsKnoten) {
		if (loesungsKnoten == null)
			return -1;

		Knoten vorgaenger = loesungsKnoten;

		while (vorgaenger.getVorgaenger() != null) {
			pacmanActionList.push(erzeugePacmanAction(vorgaenger));
			vorgaenger = vorgaenger.getVorgaenger();
		}

		return pacmanActionList.size();
	}

	private PacmanAction erzeugePacmanAction(Knoten knoten) {
		switch (knoten.GetBewegungsrichtung()) {
			case Knoten.NORD:
				return PacmanAction.GO_NORTH;
			case Knoten.OST:
				return PacmanAction.GO_EAST;
			case Knoten.SUED:
				return PacmanAction.GO_SOUTH;
			case Knoten.WEST:
				return PacmanAction.GO_WEST;
			default:
				return null;
		}
	}

	private Knoten popOpenlist (){
		Knoten besterKnoten;
		if (!openList.isEmpty()) {
			besterKnoten = openList.get(0);
			openList.remove(0);

			return besterKnoten;
		}
		return null;
	}

	private void pushCloselist (Knoten expandierterKnoten){
		closedList.add(expandierterKnoten);
	}
}
