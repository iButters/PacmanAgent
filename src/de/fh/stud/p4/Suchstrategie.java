package de.fh.stud.p4;

public enum Suchstrategie {
    Tiefensuche,
    Breitensuche,
    GreedySearch,
    UCS,
    A_Stern
}
