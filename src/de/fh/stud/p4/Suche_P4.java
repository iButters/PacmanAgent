package de.fh.stud.p4;

import de.fh.pacman.PacmanPercept;
import de.fh.pacman.enums.PacmanAction;
import de.fh.pacman.enums.PacmanTileType;
import de.fh.stud.p1.Knoten;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Suche_P4 {
    /*
     * TODO Praktikum 3 [1]: Erweitern Sie diese Klasse um die notwendigen
     * Attribute und Methoden um eine Tiefensuche durchführen zu können.
     * Die Erweiterung um weitere Suchstrategien folgt in Praktikum 4.
     */
    private Knoten loesungsKnoten;

    private Suchstrategie suchstrategie;

    private PacmanPercept pacmanPercept;

    public List<Knoten> openList = new ArrayList<Knoten>();

    private List<Knoten> closedList = new ArrayList<Knoten>();

    private Stack<PacmanAction> pacmanActionList = new Stack<PacmanAction>();
    public PacmanAction getNextAction(){
        if(this.pacmanActionList.size() != 0){
            return this.pacmanActionList.pop();
        }
        else {
            return null;
        }
    }

    /*
     * TODO Praktikum 4 [1]: Erweitern Sie diese Klasse um weitere Suchstrategien (siehe Aufgabenblatt)
     * zu unterstützen.
     */
    public Suche_P4(Knoten loesungsKnoten, PacmanPercept pacmanPercept, Suchstrategie suchstrategie){
        this.loesungsKnoten = loesungsKnoten;
        this.pacmanPercept = pacmanPercept;
        this.suchstrategie = suchstrategie;
    }

    public Knoten start() {
        /*
         * TODO Praktikum 3 [2]: Implementieren Sie hier den Algorithmus einer Tiefensuche.
         */
        Knoten startKnoten = new Knoten(pacmanPercept.getView(), pacmanPercept.getPosition().getX(), pacmanPercept.getPosition().getY());
        knotenEinfuegen(startKnoten);

        while (!openList.isEmpty()) {
            Knoten expansionsKandidat = this.popOpenlist();
            this.pushCloselist(expansionsKandidat);

            if (expansionsKandidat.isKnotenZiel(this.loesungsKnoten)) {
                Knoten loesungsKnoten = expansionsKandidat;
                int sizeResult = loesungsweg(loesungsKnoten);

                System.out.println("Länge des Weges: " + sizeResult);
                System.out.println("OpenList: " + this.openList.size() + " - ClosedList: " + this.closedList.size());

                return loesungsKnoten;
            }
            else {
                expandiereKnoten(expansionsKandidat);
            }
        }

        return null;
    }

    private void expandiereKnoten(Knoten vorgaenger) {
        List<Knoten> nachfolger = vorgaenger.expand();

        for(Knoten nachfolgerKnoten : nachfolger){
            boolean isInClosedList = false;

            knotenBewerten(nachfolgerKnoten);

            for (Knoten closedListKnoten : closedList) {
                if (nachfolgerKnoten.isKnotenGleich(closedListKnoten)) {
                    isInClosedList = true;
                }
            }

            if (!isInClosedList) {
                knotenEinfuegen(nachfolgerKnoten);
            }
        }
    }

    private void knotenEinfuegen(Knoten vorgaenger){
        switch(suchstrategie){
            case Tiefensuche:
                knotenEinfuegen_Tiefensuche(vorgaenger);
                break;
            case Breitensuche:
                knotenEinfuegen_Breitensuche(vorgaenger);
                break;
            case UCS:
                knotenEinfuegen_UCS(vorgaenger);
                break;
            case A_Stern:
                knotenEinfuegen_A_Stern(vorgaenger);
                break;
            case GreedySearch:
                knotenEinfuegen_GreedySearch(vorgaenger);
                break;
        }
    }

    private void knotenEinfuegen_Tiefensuche(Knoten expansionsKandidat) {
        this.openList.add(0, expansionsKandidat);
    }

    private void knotenEinfuegen_Breitensuche(Knoten expansionsKandidat){
        this.openList.add(expansionsKandidat);
    }

    private void knotenEinfuegen_UCS(Knoten expansionsKandidat){
        Bewertung expansionsKandidatBewertungbewertung = expansionsKandidat.getBewertung();

        Knoten knoten;
        Bewertung knotenBewertung;
        for(int i = 0; i < this.openList.size(); i++){
            knoten = this.openList.get(i);
            knotenBewertung = knoten.getBewertung();

            if(expansionsKandidatBewertungbewertung.getWegKosten() <= knotenBewertung.getWegKosten()){
                this.openList.add(i, expansionsKandidat);

                return;
            }
        }

        this.openList.add(expansionsKandidat);
    }

    private void knotenEinfuegen_A_Stern(Knoten expansionsKandidat){
        Bewertung expansionsKandidatBewertungbewertung = expansionsKandidat.getBewertung();

        Knoten knoten;
        Bewertung knotenBewertung;
        for(int i = 0; i < this.openList.size(); i++){
            knoten = this.openList.get(i);
            knotenBewertung = knoten.getBewertung();

            if(expansionsKandidatBewertungbewertung.getSchaetzwert() <= knotenBewertung.getSchaetzwert()){
                this.openList.add(i, expansionsKandidat);

                return;
            }
        }

        this.openList.add(expansionsKandidat);
    }

    private void knotenEinfuegen_GreedySearch(Knoten expansionsKandidat){
        Bewertung expBewertung = expansionsKandidat.getBewertung();

        Knoten knoten;
        Bewertung knotenBewertung;
        for(int i = 0; i < this.openList.size(); i++){
            knoten = this.openList.get(i);
            knotenBewertung = knoten.getBewertung();

            if(expBewertung.getAnzahlDots() <= knotenBewertung.getAnzahlDots()){
                this.openList.add(i, expansionsKandidat);

                return;
            }
        }

        this.openList.add(expansionsKandidat);
    }

    private void knotenBewerten(Knoten expansionsKandidat){
        switch (suchstrategie){
            case Tiefensuche:
                break;
            case Breitensuche:
                break;
            case UCS:
                knotenBewerten_UCS(expansionsKandidat);
                break;
            case A_Stern:
                knotenBewerten_A_Stern(expansionsKandidat);
                break;
            case GreedySearch:
                knotenBewerten_GreedySearch(expansionsKandidat);
                break;
        }
    }

    private void knotenBewerten_UCS(Knoten expansionsKandidat){
        Bewertung expbewertung = expansionsKandidat.getBewertung();

        Knoten expVorg = expansionsKandidat.getVorgaenger();
        Bewertung vorgBewertung = expVorg.getBewertung();

        float vorgPfadkosten = vorgBewertung != null ? vorgBewertung.getWegKosten() : 0;

        PacmanTileType[][] expView = expansionsKandidat.GetWelt();

        expbewertung.setWegKosten(vorgPfadkosten);
    }

    private void knotenBewerten_A_Stern(Knoten expansionsKandidat) {
        Bewertung expbewertung = expansionsKandidat.getBewertung();

        Knoten expVorg = expansionsKandidat.getVorgaenger();
        Bewertung vorgBewertung = expVorg.getBewertung();

        float vorgPfadkosten = vorgBewertung != null ? vorgBewertung.getWegKosten() : 0;

        PacmanTileType[][] expView = expansionsKandidat.GetWelt();
        int anzDots = 0;

        for(int i = 0; i < expView.length; i++){
            for(int j = 0; j < expView[i].length; j++){
                if(expView[i][j] == PacmanTileType.DOT)
                    anzDots++;
            }
        }

        expbewertung.setWegKosten(vorgPfadkosten);
        expbewertung.setAnzahlDots(anzDots);
    }

    private void knotenBewerten_GreedySearch(Knoten expansionsKandidat){
        Bewertung expBewertung =  expansionsKandidat.getBewertung();

        PacmanTileType[][] expView = expansionsKandidat.GetWelt();
        int anzDots = 0;

        for(int i = 0; i < expView.length; i++){
            for(int j = 0; j < expView[i].length; j++){
                if(expView[i][j] == PacmanTileType.DOT)
                    anzDots++;
            }
        }

        expBewertung.setAnzahlDots(anzDots);
    }

    private int loesungsweg(Knoten loesungsKnoten) {
        Knoten vorgaenger = loesungsKnoten;

        while (vorgaenger.getVorgaenger() != null) {
            pacmanActionList.push(erzeugePacmanAction(vorgaenger));
            vorgaenger = vorgaenger.getVorgaenger();
        }

        return pacmanActionList.size();
    }

    private PacmanAction erzeugePacmanAction(Knoten knoten) {
        switch (knoten.GetBewegungsrichtung()) {
            case Knoten.NORD:
                return PacmanAction.GO_NORTH;
            case Knoten.OST:
                return PacmanAction.GO_EAST;
            case Knoten.SUED:
                return PacmanAction.GO_SOUTH;
            case Knoten.WEST:
                return PacmanAction.GO_WEST;
            default:
                return null;
        }
    }

    private Knoten popOpenlist (){
        Knoten besterKnoten;
        if (!openList.isEmpty()) {
            besterKnoten = openList.get(0);
            openList.remove(0);

            return besterKnoten;
        }
        return null;
    }

    private void pushCloselist (Knoten expandierterKnoten){
        closedList.add(expandierterKnoten);
    }
}
