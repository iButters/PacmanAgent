package de.fh.stud.p1;

import de.fh.pacman.enums.PacmanAction;
import de.fh.stud.p4.Bewertung;
import de.fh.util.Vector2;
import de.fh.pacman.enums.PacmanTileType;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Knoten {
	/*
	 * TODO Praktikum 1 [1]: Erweitert diese Klasse um die notwendigen
	 * Attribute, Methoden und Konstruktoren um die möglichen verschiedenen Weltzustände darstellen und
	 * einen Suchbaum aufspannen zu können.
	 */

	public static final int NORD  = 1;
	public static final int OST  = 2;
	public static final int SUED  = 3;
	public static final int WEST  = 4;

	private Knoten Vorgaenger;
	public Knoten getVorgaenger(){
		return this.Vorgaenger;
	}

	private List<Knoten> Knoten = new ArrayList<Knoten>();

	private PacmanTileType[][] Welt;
	private void SetWelt(PacmanTileType[][] welt){
		for(int i = 0; i < welt.length; i++){
			for(int j= 0; j < welt[0].length; j++){
				this.Welt[i][j] = welt[i][j];
			}
		}
	}

	public PacmanTileType[][] GetWelt(){
		return this.Welt;
	}

	private Vector2 Position;
	public Vector2 GetPosition(){
		return this.Position;
	}

	private int Bewegungsrichtung;
	public int GetBewegungsrichtung(){
		return this.Bewegungsrichtung;
	}

	private Bewertung bewertung = new Bewertung();
	public Bewertung getBewertung(){
		return this.bewertung;
	}

	public Knoten(PacmanTileType[][] welt, int posX, int posY){
		this.Vorgaenger = null;
		this.Position = new Vector2(posX, posY);
		this.Bewegungsrichtung = -1;

		this.Welt = new PacmanTileType[welt.length][welt[0].length];
		SetWelt(welt);
	}

	private Stack<PacmanAction> actions = new Stack<PacmanAction>();
	public PacmanAction getNextAction(){
		return this.actions.pop();
	}

	public Knoten(Knoten vorgaenger, int bewegungsRichtung) {
		this.Vorgaenger = vorgaenger;

		if(bewegungsRichtung < NORD || bewegungsRichtung > WEST)
			throw new IllegalArgumentException("Keine gültige Bewegungsrichtung");
		this.Bewegungsrichtung = bewegungsRichtung;

		this.Welt = new PacmanTileType[vorgaenger.GetWelt().length][vorgaenger.GetWelt()[0].length];
		SetWelt(vorgaenger.GetWelt());

		this.Position = BerechneNeuePosition(vorgaenger.Position, bewegungsRichtung);

		this.Welt[this.Position.getX()][this.Position.getY()] = PacmanTileType.EMPTY;
	}

	public List<Knoten> expand() {
		/*
		 * TODO Praktikum 1 [2]: Implementiert in dieser Methode das Expandieren des Knotens.
		 * Die Methode soll die neu erzeugten Knoten (die Kinder des Knoten) zurückgeben.
		 */

		for(int i = 1; i <= 4; i++){
			Vector2 neuePosition = BerechneNeuePosition(this.Position, i);
			if(this.Welt[neuePosition.getX()][neuePosition.getY()] == PacmanTileType.WALL)
				continue;

			Knoten neuerKnoten = new Knoten(this, i);
			if(neuerKnoten != null){
				this.Knoten.add(neuerKnoten);
			}
		}

		return this.Knoten;
	}

	public boolean isKnotenGleich(Knoten knoten) {
		for(int i = 0; i < this.Welt.length; i++){
			for(int j = 0; j < this.Welt[i].length; j++){
				if(knoten.GetWelt()[i][j] != this.Welt[i][j] || knoten.GetPosition().getX() != this.GetPosition().getX() || knoten.GetPosition().getY() != this.GetPosition().getY()){
					return false;
				}
			}
		}

		return true;
	}

	public boolean isKnotenZiel(Knoten knoten){
		for(int i = 0; i < this.Welt.length; i++){
			for(int j = 0; j < this.Welt[0].length; j++){
				if(this.GetWelt()[i][j] == PacmanTileType.PACMAN || knoten.GetWelt()[i][j] == PacmanTileType.PACMAN){
					continue;
				}
				else if(knoten.GetWelt()[i][j] != this.Welt[i][j]){
					return false;
				}
			}
		}

		return true;
	}

	private Vector2 BerechneNeuePosition(Vector2 position, int bewegungsrichtung){
		Vector2 neuePosition = new Vector2(-1, -1);

		switch (bewegungsrichtung) {
			case NORD:
				neuePosition.set(position.getX(), position.getY() - 1);
				break;
			case OST:
				neuePosition.set(position.getX() + 1, position.getY());
				break;
			case SUED:
				neuePosition.set(position.getX(), position.getY() + 1);
				break;
			case WEST:
				neuePosition.set(position.getX() - 1, position.getY());
				break;

			default:
				break;
		}

		return neuePosition;
	}

	public String toString() {
		String str = "";

		str += "\n";
		str += "[" + this.Position.getX()+"," + this.Position.getY() + "]\n";
		for (int x = 0; x < Welt[0].length; x++) {
			for (int y = 0; y < Welt.length; y++) {
				str += Welt[y][x] + " ";
			}
			str += "\n";
		}

		return str;
	}
}